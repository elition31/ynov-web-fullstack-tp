// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDD5FrR7EDx1LBu3jBQJ5UemCBWteWROoE",
    authDomain: "raie-creation.firebaseapp.com",
    databaseURL: "https://raie-creation.firebaseio.com",
    projectId: "raie-creation",
    storageBucket: "raie-creation.appspot.com",
    messagingSenderId: "889164430001",
    appId: "1:889164430001:web:a2bfa8f5e751c75b31c090"
  },
  url: "http://localhost:4200"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
