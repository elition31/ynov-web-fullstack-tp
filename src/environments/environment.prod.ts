export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyDD5FrR7EDx1LBu3jBQJ5UemCBWteWROoE",
    authDomain: "raie-creation.firebaseapp.com",
    databaseURL: "https://raie-creation.firebaseio.com",
    projectId: "raie-creation",
    storageBucket: "raie-creation.appspot.com",
    messagingSenderId: "889164430001",
    appId: "1:889164430001:web:a2bfa8f5e751c75b31c090"
  },
  url: "http://localhost:4200"
};
