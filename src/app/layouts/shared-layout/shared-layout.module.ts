import { NgModule } from "@angular/core";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Components
import { NavigationComponent } from "./navigation/navigation.component";

@NgModule({
  declarations: [NavigationComponent],
  imports: [SharedModule],
  exports: [SharedModule, NavigationComponent]
})
export class SharedLayoutModule {}
