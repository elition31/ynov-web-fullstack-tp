// Angular
import { Component } from "@angular/core";
import { BreakpointObserver, Breakpoints } from "@angular/cdk/layout";

// Services
import { AuthService } from 'src/app/core/services/auth/auth.service';


@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent {
  // Set to true if mobile size is detected
  public isMobile: boolean;

  // Set to true if menu is open (Only in mobile)
  public isOpen: boolean = false;

  // Set to true if a user is authenticated
  public isAuth: boolean = false;

  /**
   * Constructor
   * @param breakpointObserver watch screen size modification
   * @param authService inject the authenticate service
   */
  constructor(private breakpointObserver: BreakpointObserver, private authService: AuthService) {
    // Start watching screen size modication
    this.breakpointObserver.observe([Breakpoints.Handset]).subscribe((result) => {
      this.isMobile = result.matches;
    });

    // Start watching auth state modification
    this.authService.auth$.subscribe((auth) => {
      if (auth) {
        this.isAuth = true;
      } else {
        this.isAuth = false;
      }
    });
  }

  /**
   * Open or close the menu
   */
  public toggle(): void {
    if (this.isMobile) {
      this.isOpen = !this.isOpen;
    }
  }

  /**
   * Log Out the user
   */
  public logout(): void {
    this.authService.logout();
  }
}
