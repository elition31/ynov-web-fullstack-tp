import { NgModule } from "@angular/core";

// Modules
import { SharedLayoutModule } from "./shared-layout/shared-layout.module";
import { UserLayoutModule } from "./user-layout/user-layout.module";
import { GuestLayoutModule } from "./guest-layout/guest-layout.module";

@NgModule({
  declarations: [],
  imports: [SharedLayoutModule, GuestLayoutModule, UserLayoutModule],
  exports: [SharedLayoutModule, GuestLayoutModule, UserLayoutModule]
})
export class LayoutsModule {}
