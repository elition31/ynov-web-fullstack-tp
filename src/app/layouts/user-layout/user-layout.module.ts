import { NgModule } from "@angular/core";

// Modules
import { SharedLayoutModule } from "../shared-layout/shared-layout.module";

// Components
import { UserLayoutComponent } from "./user-layout.component";
import { UserFooterComponent } from "./user-footer/user-footer.component";
import { UserHeaderComponent } from "./user-header/user-header.component";

@NgModule({
  declarations: [UserFooterComponent, UserHeaderComponent, UserLayoutComponent],
  imports: [SharedLayoutModule],
  exports: [UserLayoutComponent]
})
export class UserLayoutModule {}
