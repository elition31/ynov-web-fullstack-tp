// Angular
import { NgModule } from "@angular/core";

// Main Routing Module
import { AppRoutingModule } from "./app-routing.module";

// Firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { AngularFireAuthModule } from "@angular/fire/auth";

// Modules
import { CoreModule } from "./core/core.module";
import { SharedModule } from "./shared/shared.module";
import { LayoutsModule } from "./layouts/layouts.module";

// Components
import { AppComponent } from "./app.component";

// Environments
import { environment } from "src/environments/environment";

@NgModule({
  declarations: [AppComponent],
  imports: [
    SharedModule,
    CoreModule,
    AppRoutingModule,
    LayoutsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
