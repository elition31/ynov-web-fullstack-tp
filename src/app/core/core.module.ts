// Angular
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

@NgModule({
  declarations: [],
  imports: [SharedModule],
  exports: [BrowserModule, HttpClientModule, BrowserAnimationsModule]
})
export class CoreModule {}
