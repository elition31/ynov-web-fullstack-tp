/**
 * Convert a timestamp in seconds into Date
 */
export const timestampToDate = (timestamp: number): Date => {
  return new Date(timestamp * 1000);
};

/**
 * Convert and format a number into a string
 */
export const formatNumber = (value: number): string => {
  return value < 10 ? `0${value}` : `${value}`;
};

/**
 * Format a date to be read easily
 */
export const formatDate = (date: Date): string => {
  const year: number = date.getFullYear();
  const month: string = formatNumber(date.getMonth() + 1);
  const day: string = formatNumber(date.getDate());

  return `${year}-${month}-${day}`;
};

/**
 * Format a date to be read easily
 */
export const formatTime = (date: Date): string => {
  const hour: string = formatNumber(date.getHours());
  const minute: string = formatNumber(date.getMinutes());

  return `${hour}h${minute}`;
};

/**
 * Add days and return the new date
 * @param date the date to add days
 * @param days the number of days you need to add
 */
export const addDays = (date: Date, days: number): Date => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

