export interface ServiceErrors {
  auth: string | null;
  user: string | null;
  toast: string | null;
}