import { User } from "firebase";

export interface UserInfos {
  uid?: string;
  firstname?: string;
  lastname?: string;
  phoneNumber?: string;
  rdv?: any;
  favoriteLocation?: string[];
  createdAt?: any;
  updatedAt?: any;
}
