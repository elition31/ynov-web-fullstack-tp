export interface Location {
  id: string;
  name: string;
  address: string;
  city: string;
  phoneNumber: string;
}