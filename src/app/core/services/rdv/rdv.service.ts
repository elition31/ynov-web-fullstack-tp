// Angular
import { Injectable } from "@angular/core";

// Firebase
import { AngularFirestore } from "@angular/fire/firestore";

// Services
import { ErrorsService } from "../error/errors.service";
import { ToastService } from "../toast/toast.service";
import { Observable } from "rxjs";

// Models
import { Rdv } from "../../models/rdv.interface";
import { Location } from "../../models/location.interface";
import { addDays } from "../../utils/date";

@Injectable({
  providedIn: "root"
})
export class RdvService {
  constructor(private db: AngularFirestore, private errorsService: ErrorsService, private toastService: ToastService) {}

  /**
   * Fetch every shops
   */
  public fetchLocations(): Observable<Location[]> {
    return this.db.collection<Location>("locations").valueChanges();
  }

  /**
   * Fetch every appointments in the next 7 days for the shop given
   * @param id the ID of the shop
   */
  public fetchRdvs(id: string): Observable<Rdv[]> {
    const now: Date = new Date();
    const today: Date = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 8, 0, 0);
    const futur: Date = addDays(today, 7);

    return this.db
      .collection<Rdv>("rdv", (ref) =>
        ref
          .where("date", ">=", today)
          .where("date", "<=", futur)
          .where("locationId", "==", id)
      )
      .valueChanges();
  }

  /**
   * Create an appointment
   * @param rdv an appointment
   */
  public createRdv(rdv: Rdv): Promise<void> {
    return this.db
      .collection<Rdv>("rdv")
      .doc<Rdv>(this.db.createId())
      .set(rdv);
  }
}
