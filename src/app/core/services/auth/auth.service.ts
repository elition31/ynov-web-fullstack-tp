// Angular
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

// Rxjs
import { BehaviorSubject } from "rxjs";

// Firebase
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";

// Models
import { UserInfos } from "../../models/user-infos.interface";
import { AuthInfos } from "../../models/auth-infos.interface";

// Data
import { environment } from "src/environments/environment";

// Services
import { ErrorsService } from "../error/errors.service";
import { ToastService } from "../toast/toast.service";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  public auth$: BehaviorSubject<AuthInfos> = new BehaviorSubject<AuthInfos>(null);
  public isLoading$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  /**
   * Constructor
   * @param afAuth Handle Firebase Authentification
   * @param db Handle Firebase Database
   * @param router Handle routing manipulation
   * @param errorsService Handle application's errors
   * @param toastService Handle toast notifications in the application
   */
  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private router: Router,
    private errorsService: ErrorsService,
    private toastService: ToastService
  ) {
    this.afAuth.authState.subscribe(async (auth) => {
      // If user is authenticated
      if (auth) {
        // Set the app in loading state
        this.isLoading$.next(true);

        // Get user's informations from database
        const userInfos: UserInfos = (
          await this.db
            .collection<UserInfos>("users")
            .doc<UserInfos>(auth.uid)
            .get()
            .toPromise()
        ).data();

        // Set Auth and User informations for the application
        this.auth$.next({ user: auth, informations: userInfos });

        // Set the app in "ready to go" state
        this.isLoading$.next(false);

        // Redirect the user to the Dashboard page
        this.router.navigateByUrl("/user/rdv");
      } else {
        // Set data of the Behavior Subject to null
        this.auth$.next(null);

        // Redirect the user to the SignIn page
        this.router.navigateByUrl("/signin");
      }
    });
  }

  /**
   * Handle user authentication
   * @param email A valid email
   * @param password A restrictive password
   */
  public async login(email: string, password: string): Promise<any> {
    try {
      // Log In the user
      await this.afAuth.auth.signInWithEmailAndPassword(email, password);

      // Display a message
      this.toastService.open("Bon retour parmis nous !");
    } catch (error) {
      this.errorsService.addError(error.message, "auth");
    }
  }

  /**
   * Allow user to register
   * @param email A valid email
   * @param password A restrictive password
   */
  public async register(email: string, password: string): Promise<any> {
    try {
      // Create user account
      const registration = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);

      // Initialize user info
      const userInfos: UserInfos = {
        uid: registration.user.uid,
        firstname: "",
        lastname: "",
        phoneNumber: "",
        favoriteLocation: [],
        createdAt: new Date(),
        updatedAt: new Date()
      };

      // Set User's informations in database
      this.db
        .collection("users")
        .doc<UserInfos>(registration.user.uid)
        .set(userInfos);

      // Set Auth and User informations for the application
      this.auth$.next({ user: registration.user, informations: userInfos });

      // Display a message
      this.toastService.open("Bienvenue parmis nous !");
    } catch (error) {
      this.errorsService.addError(error.message, "auth");
    }
  }

  /**
   * Allow user to reset his password when he is not log in
   * @param email A valid email
   */
  public async reset(email: string): Promise<any> {
    try {
      // Send an email to allow user to reset his password
      await this.afAuth.auth.sendPasswordResetEmail(email, { url: environment.url });

      // Display a message
      this.toastService.open("Un email pour réinitialiser votre mot de passe vous a été envoyé.");

      // Redirect the user to the SignIn page
      this.router.navigateByUrl("/signin");
    } catch (error) {
      this.errorsService.addError(error.message, "auth");
    }
  }

  /**
   * Allow user to log out from the application
   */
  public async logout(): Promise<any> {
    try {
      // Log Out the user
      await this.afAuth.auth.signOut();

      // Remove data of the Behavior Subject
      this.auth$.next(null);

      // Display a message
      this.toastService.open("À bientôt !");
    } catch (error) {
      this.errorsService.addError(error.message, "auth");
    }
  }
}
