// Angular
import { Injectable } from "@angular/core";

// Angular Material
import { MatSnackBar } from "@angular/material/snack-bar";

// Services
import { ErrorsService } from "../error/errors.service";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

@Injectable({
  providedIn: SharedModule
})
export class ToastService {
  constructor(private toast: MatSnackBar, private errorsService: ErrorsService) {
    this.errorsService.errors$.subscribe((errors) => {
      for (const i in errors) {
        if (errors.hasOwnProperty(i) && errors[i] !== null) {
          this.displayError(errors[i], i);
        }
      }
    });
  }

  public displayError(message: string, service: string) {
    this.toast
      .open(message, "ok", {
        duration: 3000,
        horizontalPosition: "right",
        verticalPosition: "bottom",
        panelClass: ["toast-error"]
      })
      .afterDismissed()
      .toPromise()
      .finally(() => {
        this.errorsService.deleteError(service);
      });
  }

  public open(message: string, type: "info" | "success" = "info", duration: number = 3000, button: string = "ok") {
    this.toast.open(message, button, {
      duration,
      horizontalPosition: "right",
      verticalPosition: "bottom",
      panelClass: type === "info" ? "toast-info" : "toast-success"
    });
  }
}
