// Angular
import { Injectable } from "@angular/core";

// Firebase
import { AngularFirestore } from "@angular/fire/firestore";

// Services
import { AuthService } from "../auth/auth.service";
import { ErrorsService } from "../error/errors.service";
import { ToastService } from "../toast/toast.service";

// Models
import { UserInfos } from "../../models/user-infos.interface";

@Injectable({
  providedIn: "root"
})
export class UserService {
  constructor(
    private authService: AuthService,
    private db: AngularFirestore,
    private errorsService: ErrorsService,
    private toastService: ToastService
  ) {}

  /**
   * Allow user to update his password when he is log in
   * @param password A valid password
   */
  public async updatePassword(password: string): Promise<void> {
    try {
      // Update user's password
      await this.authService.auth$.value.user.updatePassword(password);

      const user: UserInfos = this.authService.auth$.value.informations;

      // Change the value of update
      await this.db
        .collection<UserInfos>("users")
        .doc<Partial<UserInfos>>(this.authService.auth$.value.informations.uid)
        .set({ uid: user.uid, updatedAt: new Date() }, { merge: true });

      // Display a message
      this.toastService.open("Votre mot de passe a été modifié.");
    } catch (error) {
      this.errorsService.addError(error.message, "user");
    }
  }

  /**
   * Allow user to update his profile's informations
   * @param user A partial variable
   */
  public updateUser(user: Partial<UserInfos>): Promise<void> {
    return this.db
      .collection<UserInfos>("users")
      .doc<Partial<UserInfos>>(user.uid)
      .set(user, { merge: true });
  }
}
