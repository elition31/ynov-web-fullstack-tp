// Angular
import { Component } from "@angular/core";

// Services
import { ToastService } from "./core/services/toast/toast.service";
import { AuthService } from "./core/services/auth/auth.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"]
})
export class AppComponent {
  public authIsLoaded: boolean;

  constructor(public toastService: ToastService, private authService: AuthService) {
    this.authService.isLoading$.subscribe((value) => {
      this.authIsLoaded = value;
    });
  }
}
