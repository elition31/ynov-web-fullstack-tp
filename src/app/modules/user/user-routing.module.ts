// Angular
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

// Components
import { ProfileComponent } from "./pages/profile/profile.component";
import { RdvComponent } from './pages/rdv/rdv.component';

const routes: Routes = [
  {
    path: "",
    redirectTo: "rdv"
  },
  {
    path: "",
    children: [
      {
        path: "rdv",
        component: RdvComponent
      },
      {
        path: "profile",
        component: ProfileComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
