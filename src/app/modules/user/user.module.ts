// Angular
import { NgModule } from "@angular/core";

// Routing Module
import { UserRoutingModule } from "./user-routing.module";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Pages
import { ProfileComponent } from "./pages/profile/profile.component";
import { RdvComponent } from './pages/rdv/rdv.component';
import { AddComponent } from './pages/rdv/dialogs/add/add.component';

@NgModule({
  declarations: [ProfileComponent, RdvComponent, AddComponent],
  imports: [SharedModule, UserRoutingModule],
  entryComponents: [AddComponent]
})
export class UserModule {}
