// Angular
import { Component } from "@angular/core";
import { switchMap } from "rxjs/operators";

// Angular Material
import { MatDialog } from "@angular/material/dialog";

// Service
import { RdvService } from "src/app/core/services/rdv/rdv.service";
import { ToastService } from "src/app/core/services/toast/toast.service";

// Models
import { Rdv } from "src/app/core/models/rdv.interface";
import { Location } from "src/app/core/models/location.interface";

// Dialogs
import { AddComponent } from "./dialogs/add/add.component";

// Utils
import { timestampToDate, formatDate, formatTime } from "src/app/core/utils/date";

// Calendar
import dayGridPlugin from "@fullcalendar/daygrid";

@Component({
  selector: "app-rdv",
  templateUrl: "./rdv.component.html",
  styleUrls: ["./rdv.component.scss"]
})
export class RdvComponent {
  // Set the plugin used in calendar
  public calendarPlugins = [dayGridPlugin];

  // A list of RDV
  public events: any[];

  // A list of every locations
  public locations: Location[] = [];

  // The selected location
  public locationId: string = "";

  /**
   * Constructor
   * @param rdvService Handle RDV
   * @param dialog Allow component to open Dialogs
   */
  constructor(private rdvService: RdvService, private dialog: MatDialog, private toastService: ToastService) {
    this.prepareData();
  }

  /**
   * Allow user to create an appointment
   */
  public async addRDV(): Promise<void> {
    const dialog = this.dialog.open(AddComponent, { minWidth: "450px" });

    dialog.afterClosed().subscribe((result: Rdv | undefined) => {
      if (result) {
        this.rdvService.createRdv(result);
        this.toastService.open("Votre Rendez-vous est prit.");
      }
    });
  }

  /**
   * Prepare data by requesting the database
   */
  private prepareData(): void {
    const observe = this.rdvService.fetchLocations().pipe(switchMap((locations) => (this.locations = locations)));

    observe.subscribe((result) => {
      this.locationId = this.locations[0].id;
      this.setEvents();
    });
  }

  /**
   * Method called to set the events on the calendar
   */
  public setEvents(): void {
    this.rdvService.fetchRdvs(this.locationId).subscribe((events) => {
      this.events = [];
      for (const event of events) {
        const date = timestampToDate(event.date.seconds);
        this.events.push({ title: formatTime(date), date: formatDate(date) });
      }
    });
  }
}
