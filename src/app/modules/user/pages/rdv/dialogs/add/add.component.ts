// Angular
import { Component } from "@angular/core";
import { FormGroup, Validators, AbstractControl, FormControl } from "@angular/forms";

// Angular Material
import { MatDialogRef } from "@angular/material/dialog";

// Services
import { AuthService } from "src/app/core/services/auth/auth.service";
import { RdvService } from "src/app/core/services/rdv/rdv.service";

// Models
import { Rdv } from "src/app/core/models/rdv.interface";
import { Location } from 'src/app/core/models/location.interface';

@Component({
  selector: "app-add",
  templateUrl: "./add.component.html",
  styleUrls: ["./add.component.scss"]
})
export class AddComponent {
  // Form
  public form: FormGroup;

  // Locations List
  public locations: Location[] = [];

  // Set to true when ready to be displayed
  isReady: boolean = false;

  /**
   * Constructor
   * @param dialogRef Handle dialog box
   * @param authService Handle auth service
   * @param rdvService Handle RDV service
   */
  constructor(public dialogRef: MatDialogRef<AddComponent>, private authService: AuthService, private rdvService: RdvService) {
    this.authService.auth$.subscribe((auth) => {
      this.rdvService.fetchLocations().subscribe((result) => {
        this.locations = result;
        this.isReady = true;
      });

      this.form = new FormGroup({
        date: new FormControl( new Date(), [Validators.required]),
        ownerId: new FormControl(auth.user.uid, [Validators.required]),
        locationId: new FormControl(auth.informations.favoriteLocation, [Validators.required])
      });
    });
  }

  get date(): AbstractControl {
    return this.form.get("date");
  }

  get ownerId(): AbstractControl {
    return this.form.get("ownerId");
  }

  get locationId(): AbstractControl {
    return this.form.get("locationId");
  }

  /**
   * Method is called when the form is submited
   */
  public handleSubmit() {
    const rdv: Rdv = {
      date: new Date(this.date.value),
      ownerId: this.ownerId.value,
      locationId: this.locationId.value
    };

    this.dialogRef.close(rdv);
  }
}
