// Angular
import { NgModule } from "@angular/core";

// Routing Module
import { GuestRoutingModule } from "./guest-routing.module";

// Modules
import { SharedModule } from "src/app/shared/shared.module";

// Components
import { SigninComponent } from "./pages/signin/signin.component";
import { SignupComponent } from "./pages/signup/signup.component";
import { ResetPasswordComponent } from "./pages/reset-password/reset-password.component";

@NgModule({
  declarations: [SigninComponent, SignupComponent, ResetPasswordComponent],
  imports: [SharedModule, GuestRoutingModule]
})
export class GuestModule {}
