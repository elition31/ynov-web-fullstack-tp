// Angular
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, AbstractControl } from "@angular/forms";

// Services
import { AuthService } from "src/app/core/services/auth/auth.service";

@Component({
  selector: "app-signin",
  templateUrl: "./signin.component.html",
  styleUrls: ["./signin.component.scss"]
})
export class SigninComponent implements OnInit {
  public form: FormGroup;
  public loading: boolean = false;

  constructor(private authService: AuthService, private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: ["", [Validators.minLength(6), Validators.required]]
    });
  }

  get email(): AbstractControl {
    return this.form.get("email");
  }

  get password(): AbstractControl {
    return this.form.get("password");
  }

  public async onSubmit(): Promise<void> {
    this.authService.login(this.email.value, this.password.value);
  }
}
