// Angular
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, AbstractControl } from "@angular/forms";

// Services
import { AuthService } from "src/app/core/services/auth/auth.service";

@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {
  public form: FormGroup;
  public loading: boolean = false;

  constructor(private authService: AuthService, private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]],
      password: [
        "",
        [Validators.minLength(6), Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}")]
      ],
      confirmPassword: [
        "",
        [Validators.minLength(6), Validators.required, Validators.pattern("(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{6,}")]
      ]
    });
  }

  get email(): AbstractControl {
    return this.form.get("email");
  }

  get password(): AbstractControl {
    return this.form.get("password");
  }

  get confirmPassword(): AbstractControl {
    return this.form.get("confirmPassword");
  }

  public passwordDoestMatch(): boolean {
    return this.password.value === this.confirmPassword.value;
  }

  public async onSubmit(): Promise<void> {
    this.authService.register(this.email.value, this.password.value);
  }
}
