// Angular
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators, AbstractControl } from "@angular/forms";

// Services
import { AuthService } from "src/app/core/services/auth/auth.service";

@Component({
  selector: "app-reset-password",
  templateUrl: "./reset-password.component.html",
  styleUrls: ["./reset-password.component.scss"]
})
export class ResetPasswordComponent implements OnInit {
  public form: FormGroup;

  constructor(private authService: AuthService, private fb: FormBuilder) {}

  ngOnInit() {
    this.form = this.fb.group({
      email: ["", [Validators.required, Validators.email]]
    });
  }

  get email(): AbstractControl {
    return this.form.get("email");
  }

  public async onSubmit(): Promise<void> {
    this.authService.reset(this.email.value);
  }
}
